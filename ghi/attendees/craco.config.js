module.exports = {
    webpack: {
      configure: (webpackConfig, { env, paths }) => {
        // modify the webpackConfig here
        module.exports = {
            //...
            watchOptions: {
              poll: 1000, // Check for changes every second
            }
          };
        return webpackConfig;
      }
    }
  };