import Nav from './Nav'

function App(props) {
  if (props.attendees === undefined) {
    return null
  }
  return (
    <>
      <Nav />
      <div className="container-fluid">
        <table className="table table-hover border">
          <thead>
            <tr className="table-dark">
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {props.attendees.map(attendee => {
              return (
                <tr key={attendee.href}>
                  <td>{ attendee.name }</td>
                  <td>{ attendee.conference }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default App;