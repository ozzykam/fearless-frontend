module.exports = {
    webpack: {
      configure: (webpackConfig, { env, paths }) => {
        webpackConfig.devServer = {
          ...(webpackConfig.devServer || {}),
          setupMiddlewares: (middlewares, devServer) => {
            // Apply custom middleware logic
            return middlewares;
          }
        };
        return webpackConfig;
      }
    }
  };