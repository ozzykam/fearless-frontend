window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'

    const responseMain = await fetch(url)
    if (responseMain.ok) {
        const data = await responseMain.json()

        const selectTag = document.getElementById('location')
        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name
            selectTag.appendChild(option)
        }
    } else {
        const message = 'Failed to FETCH LOCATIONS'
        throw new Error(message)

    }

    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const conferenceResponse = await fetch(conferenceUrl, fetchConfig)
        if (conferenceResponse.ok) {
            formTag.reset()
            const newConference = await conferenceResponse.json()
            console.log(newConference)
        } else {
            console.error('Failed to CREATE CONFERENCE')
        }
    })
})