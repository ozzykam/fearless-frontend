function createCard(name, description, pictureUrl, startDate, endDate, location, detailUrl) {
    return `
      <div class="col">
        <div class="card h-100 shadow bg-body-tertiary rounded">
          <img src="${pictureUrl}" class="card-img-top" style="max-height: 350px; overflow:hidden">
          <div class="card-body" style="display: flex; flex-direction: column; height: 100%;">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 font-weight-light text-muted fst-italic">${location}</h6>
            <p class="card-text">${description}</p>
            <a class="btn btn-primary mt-auto" href="${detailUrl}" role="button" style="align-self: flex-start;">More Info</a>
          </div>
          <div class="card-footer">
            <small class="text-body-secondary">${startDate} - ${endDate}</small>
          </div?
        </div>
      </div>
    `
  }

function runAlert(message, type) {
    return `
      <div class="alert alert=${type} alert-dismissible fade show" role="alert">
        ${message}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    
    `
}

window.addEventListener('DOMContentLoaded', async () => {
    
    const url = 'http://localhost:8000/api/conferences/'

    try {
        const response = await fetch(url)

        if (!response.ok) {
            const message = 'Failed to get CONFERENCE LIST'
            throw new Error(message)

        } else {
            const data = await response.json()

            for (let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                
                if (!detailResponse.ok) {
                    const message = 'Failed to get CONFERENCE DETAILS'
                    throw new Error(message)
                
                } else {
                    const details = await detailResponse.json()
                    
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const location = details.conference.location.name

                    const isoStartDate = new Date(details.conference.starts)
                    const startMonth = isoStartDate.getMonth() + 1
                    const startDay = isoStartDate.getDate()
                    const startYear = isoStartDate.getFullYear()
                    const startDate = `${startMonth}/${startDay}/${startYear}`

                    const isoEndDate = new Date(details.conference.ends)
                    const endMonth = isoEndDate.getMonth() + 1
                    const endDay = isoEndDate.getDate()
                    const endYear = isoEndDate.getFullYear()
                    const endDate = `${endMonth}/${endDay}/${endYear}`

                    const html = createCard(title, description, pictureUrl, startDate, endDate, location, detailUrl)
                    const card = document.querySelector('.row')
                    card.innerHTML += html
                }
            }

        }

    } catch (e) {
        console.error('Error: ', e)
        runAlert(e.message, 'danger')
    }

})

