window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'

    const responseMain = await fetch(url)
    if (responseMain.ok) {
        const data = await responseMain.json()

        const selectTag = document.getElementById('conference')
        for (let conference of data.conferences) {
            const option = document.createElement('option')
            option.value = conference.id
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
    } else {
        const message = 'Failed to FETCH CONFERENCES'
        throw new Error(message)

    }

    const formTag = document.getElementById('create-presentation-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const select = document.getElementById('conference')
        console.log(select)
        const presentationUrl = `http://localhost:8000/api/conferences/${select.selectedIndex}/presentations/`
        console.log(presentationUrl)
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        console.log("json payload:", json)
        const presentationResponse = await fetch(presentationUrl, fetchConfig)
        if (presentationResponse.ok) {
            formTag.reset()
            const newPresentation = await presentationResponse.json()
            console.log(newPresentation)
        } else {
            console.error('Failed to CREATE PRESENTATION')
        }
    })
})